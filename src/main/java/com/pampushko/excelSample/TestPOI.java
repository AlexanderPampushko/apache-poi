package com.pampushko.excelSample;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

/**
 * Created by Alexander Pampushko on 23.07.2016.
 * Создаем таблицу эксель
 * при этом строки таблицы сгруппированы
 * уровней иерархии 3
 * Как в JIRA
 * Эпики, в них находятся задачи, а в задачах - подзадачи.
 * Соответственно, подзадачи в задача сгруппированы.
 * Задачи в эпиках тоже сгруппированы.
 */
public class TestPOI
{
	//максимальное количество записей на первом уровне иерархии
	private static final int EPIC_MAX_AMOUNT = 150;
	//максимальное количество записей на втором уровне иерархии
	private static final int TASK_MAX_AMOUNT = 6;
	//максимальное количество записей не третьем уровне иерахии (последнем)
	private static final int SUBTASK_MAX_AMOUNT = 10;

	//набор строк которые станут названия колонок в формируемой нами таблице
	private static final String[] COLUMN_HEADERS = {
			"Тип задачи",
			"Тема",
			"Описание задачи",
			"Статус задачи",
			"Исполнитель задачи",
			"Подразделение заказчика",
			"Проект стратегического комитета",
			"Приоритет",
			"Месяц",
			"Плановая дата старта",
			"Первоначальная оценка",
			"Потрачено часов",
			"Осталось часов"
	};

	//количество колонок в генерируемой нами таблице
	private static final int COLUMNS = COLUMN_HEADERS.length;
	
	/**
	 * метод формирует эксель таблицу с группировками строк и заданным набором колонок
	 * значения ячеек таблицы заполнены номерами строк в которых расположены эти ячейки
	 * @param args
	 */
	public static void main(String[] args)
	{
		//создалил рендом
		Random random = new Random();

		//создали книгу эксель
		Workbook workbook = new XSSFWorkbook();
		
		//создали таблицу в книге
		Sheet sheet = workbook.createSheet();
		
		//чтобы плюсики в группировке отображались сверху, а не снизу группировки
		//Это данные - структура - Расположение итоговых данных - итоги в строках под данными - СНЯТЬ ГАЛОЧКУ!
		sheet.setRowSumsBelow(false);
		
		//создадим стиль для заголовка
		CellStyle headerStyle = createHeaderStyle(workbook);
		//создадим стиль для эпиков
		CellStyle epicStyle = createEpicStyle(workbook);
		//Создадим стиль для задач
		CellStyle taskStyle = createTaskStyle(workbook);
		//Создадим стиль подзадач
		CellStyle subtaskStyle = createsubtaskStyle(workbook);
		
		//количество наших эпиков //еще добавляем немного чтобы не было нулевое или очень малое значение
		int epicAmount = random.nextInt(EPIC_MAX_AMOUNT) + 5;

		//печатаем заголовок нашего отчета
		printHeader(sheet, 0, headerStyle);

		//общий счетчик задач
		//нумерация строк в таблице начинается с нуля
		//но поскольку мы хотим распечатать также заголовок таблицы
		//то нам следует пропустить первую строку и
		//создавать строки не начиная с номер ноль
		//а начиная с номер 1 (один)
		int rowNumber = 1;
		
		int i = 0;
		while (i < epicAmount)
		{
			//создаем строку эпика
			Row epicRow = sheet.createRow(rowNumber);

			//добавляем ячейки к строке эпика
			//(значением ячейки берем номер строки)
			String cellValue = String.valueOf(rowNumber);
			addCellsToRow(epicRow, cellValue, epicStyle);
			
			//увечиваем номер строки
			rowNumber++;
			
			//внутри эпика создаем несколько задач
			int taskAmout = random.nextInt(TASK_MAX_AMOUNT);
			int taskCounter = 0;
			//номер строки таблицы с которого мы будем начинать группировать задачи
			int startGroupTask = rowNumber;
			while (taskCounter < taskAmout)
			{
				//создаем строку задачи
				Row taskRow = sheet.createRow(rowNumber);
				addCellsToRow(taskRow, String.valueOf(rowNumber), taskStyle);
				rowNumber++;
				
				//создадим строку подзадачи содержащейся в задаче
				int subtaskAmout = random.nextInt(SUBTASK_MAX_AMOUNT);
				int subtaskCounter = 0;
				//номер строки таблицы с которого мы будем начинать группировать подзадачи
				int startGroupSubtask = rowNumber;
				while (subtaskCounter < subtaskAmout)
				{
					Row subtaskRow = sheet.createRow(rowNumber);
					addCellsToRow(subtaskRow, String.valueOf(rowNumber), subtaskStyle);
					rowNumber++;
					subtaskCounter++;
				}
				
				//номер строки таблицы на котором мы закончим группировку подзадач
				int endGroupSubtask = rowNumber;
				//если у этой задачи есть подзадачи, то группируем их
				if (subtaskAmout > 0)
				{
					sheet.groupRow(startGroupSubtask, endGroupSubtask);
				}
				taskCounter++;
			}
			int endGroup = rowNumber - 1;
			if (taskAmout > 0)
			{
				//группируем задачи
				sheet.groupRow(startGroupTask, endGroup);
			}
			i++;
		}
		
		//устанавливаем для всех колонок определение ширины по содержимому
		setAutoSizeColumns(sheet);
		
		int firstCol = 0;
		int lastCol = COLUMN_HEADERS.length - 1;
		int firstRow = sheet.getFirstRowNum();
		int lastRow = sheet.getLastRowNum();
		
		sheet.setAutoFilter(new CellRangeAddress(firstRow, lastRow, firstCol, lastCol));

		try
		{
			FileOutputStream fileOutputStream = new FileOutputStream("result1.xlsx");
			workbook.write(fileOutputStream);
			fileOutputStream.close();
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace(System.err);
		}
		catch (IOException e)
		{
			e.printStackTrace(System.err);
		}
	}
	
	/**
	 * Добавляет к строке таблицы ячейки
	 * @param row строка в которую мы хотим добавить ячеек
	 * @param cellValue все ячейки мы заполеняем одним и тем же значением. Значение берем из этого параметра
	 * @param style к ячейкам мы применяем стиль
	 */
	public static void addCellsToRow(Row row, String cellValue, CellStyle style)
	{
		for(int i = 0; i < COLUMNS; i++)
		{
			Cell cell = row.createCell(i);

			cell.setCellStyle(style);
			cell.setCellValue(cellValue);
		}
	}
	
	/**
	 * Метод печатеет заловок таблицы
	 * @param sheet таблица к которой мы печатаем заголовок
	 * @param number номер строки в которой мы этот заголовок печатаем (обычно 0, но может быть и другой номер если мы хотим может распечатать заголовок в конце или внутри таблицы)
	 * @param style стиль заголовка
	 */
	public static void printHeader(Sheet sheet, int number, CellStyle style)
	{
		Row header_row = sheet.createRow(number);
		header_row.setHeight((short) 1000);

		for(int i = 0; i < COLUMNS; i++)
		{
			Cell cell = header_row.createCell(i);
			cell.setCellStyle(style);
			cell.setCellValue(COLUMN_HEADERS[i]);
		}
	}
	
	/**
	 * Метод создает стиль для заголовка таблицы
	 * @param wb книга в которой создается стиль
	 * @return стиль, который затем можно применять к ячейкам таблицы
	 */
	public static CellStyle createHeaderStyle(Workbook wb)
	{
		CellStyle style5 = wb.createCellStyle();
		style5.setWrapText(true);

		Font headerFont = wb.createFont();
		headerFont.setFontHeightInPoints((short)11);
		headerFont.setFontName("Calibri");
		//headerFont.setBold(true);

		byte[] rgb = new byte[3];
		rgb[0] = (byte) 242; // red
		rgb[1] = (byte) 220; // green
		rgb[2] = (byte) 219; // blue
		XSSFColor myColor = new XSSFColor(rgb);
		style5.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		
		style5.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style5.setFont(headerFont);
		style5.setAlignment(CellStyle.ALIGN_CENTER);
		style5.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		return style5;
	}
	
	
	/**
	 * метод создает стиль для ячеек эпика (уровень иерархии 1)
	 * @param wb книга в которой мы создаем стиль
	 * @return стиль ячеек эпика
	 */
	public static CellStyle createEpicStyle(Workbook wb)
	{
		CellStyle style5 = wb.createCellStyle();
		Font headerFont = wb.createFont();
		headerFont.setFontHeightInPoints((short)11);
		headerFont.setFontName("Calibri");
		//headerFont.setBold(true);

		style5.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
		style5.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style5.setFont(headerFont);
		return style5;
	}
	
	/**
	 * метод создает стиль для ячеек строк. Эти строки служат для отображения задач (уровень иерархии 2)
	 * @param wb Книга в которой создается стиль
	 * @return стиль ячеек для отображения данных задачи
	 */
	public static CellStyle createTaskStyle(Workbook wb)
	{
		CellStyle style5 = wb.createCellStyle();
		Font headerFont = wb.createFont();
		headerFont.setFontHeightInPoints((short)11);
		headerFont.setFontName("Calibri");
		//headerFont.setBold(true);

		style5.setFillForegroundColor(IndexedColors.LIGHT_TURQUOISE.getIndex());
		style5.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style5.setFont(headerFont);
		return style5;
	}
	
	/**
	 * метод создает стиль для ячеек подзадач
	 * @param wb книга в которой создается стиль
	 * @return стиль для ячеек отображающих данных подзадач
	 */
	public static CellStyle createsubtaskStyle(Workbook wb)
	{
		CellStyle style5 = wb.createCellStyle();
		Font headerFont = wb.createFont();
		headerFont.setFontHeightInPoints((short)11);
		headerFont.setFontName("Calibri");
		//headerFont.setBold(true);

		style5.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		style5.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style5.setFont(headerFont);
		return style5;
	}
	
	/**
	 * метод устанавливает автоматический размер ширины колонок для таблицы
	 * исходя из их содержимого (исходя из того какой размер нужен для корректного отображения содержимого ячейки)
	 * @param sheet таблица которой мы хотим установить ширину колонок
	 */
	public static void setAutoSizeColumns(Sheet sheet)
	{
		for(int i = 0; i < COLUMNS; i++)
		{
			//настраиваем ширину колонок такой чтобы она вычислялась
			//по содержимому самой широкой ячейки в колонке
			sheet.autoSizeColumn(i);
		}
	}
}